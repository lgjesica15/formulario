<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Formulario en PHP</title>
	<!-- Agregar bulma.css -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bulma.min.css'); ?>">
</head>
<body>

	<section class="hero is-link">
		<div class="hero-body">
			<p class="title">Formulario en PHP</p>
			<p class="subtitle">20TE0550M Jesica Teresa López García</p>
		</div>
	</section>

	<section class="section">
		<div class="columns">
			<div class="column"></div>
				
			<div>
				<form action="procesar.php" method="post">
					<div>
						<label>Nombre:</label>
						<input class="input is-primary" type="text" name="nombre">
					</div>

					<div>
						<label>Apellido Paterno:</label>
						<input class="input is-primary" type="text" name="apat">
					</div>

					<div>
						<label>Apellido Materno:</label>
						<input class="input is-primary" type="text" name="amat">
					</div>

					<div>
						<label>Cargo:</label>
						<input class="input is-primary" type="text" name="carg">
					</div>

					<div>
						<label>RFC:</label>
						<input class="input is-primary" type="text" name="rfc">
					</div>

					<div>
						<label>CURP:</label>
						<input class="input is-primary" type="text" name="curp">
					</div>

					<div>
						<label>Domicilio:</label>
						<input class="input is-primary" type="text" name="domi">
					</div>

					<div>
						<label>Telefono:</label>
						<input class="input is-primary" type="text" name="tel">
					</div>

					<div>
						<label>Correo electrónico:</label>
						<input class="input is-primary" type="email" name="email">
					</div>

					<div>
						<label>Fecha de nacimiento:</label>
						<input class="input is-primary" type="text" name="fn">
					</div>
					<div class="field is-horizontal">
						<div class="field-label is-normal">
    						<label class="label">Estado Civil</label>
  						</div>
						<div class="field-body">
    						<div class="field is-narrow">
								<div class="control">
									<div class="select">
										<select name= ec>
											<option>seleccionar</option>
											<option>Casado</option>
											<option>Soltero(a)</option>
											<option>Union libre</option>
											<option>Viudo</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>	
					<div class="field is-horizontal">
						<div class="field-label">
    						<label class="label">Sexo:</label>
  						</div>
						<div class="field-body">
							<div class="field is-narrow">
								<div class="control">
									<label class="radio">
										<input type="radio" name="member">
										Masculino
									</label>
									<label class="radio">
										<input type="radio" name="member">
										Femenino
									</label>
								</div>
							</div>
						</div>
					</div>

					<div>
						<label>Referencia Familiar:</label>
						<input class="input is-primary" type="text" name="rf">
					</div>

					<div>
						<label>Observaciones:</label>
						<input class="input is-primary" type="text" name="obs">
					</div>

					<div>
						<input type="submit" class="button is-link" value="Guardar">
					</div>

				</form>

			</div>

			<div class="column"></div>
		</div>

		

	<footer class="footer">
		<div class="content has-text-centered">
			<p>
				<strong> Developed by JesicaTeresaLG</strong>
				<a href="mapaches.info">mapaches.info</a>.
			</p>
		</div>
	</footer>
</body>
</html>

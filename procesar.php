<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Formulario en PHP</title>
	<!-- Agregar bulma.css -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bulma.min.css'); ?>">
</head>
<body>

	<section class="hero is-link">
		<div class="hero-body">
			<p class="title">Formulario en PHP</p>
			<p class="subtitle">20TE0550M Jesica Teresa López García</p>
		</div>
	</section>

	<section class="section">
		<div class="columns">
			<div class="column"></div>

		    <div class="column is-three-fifths">
                <?php

                    $nombre= $_POST['nombre'];
                    $apat= $_POST['apat'];
                    $amat= $_POST['amat'];
                    $carg= $_POST['carg'];
                    $rfc= $_POST['rfc'];
                    $curp= $_POST['curp'];
                    $domi= $_POST['domi'];
                    $tel= $_POST['tel'];
                    $email= $_POST['email'];
                    $fn= $_POST['fn'];
                    $ec= $_POST['ec'];
                    $rf= $_POST['rf'];
                    $obs= $_POST['obs'];


                    echo $nombre . " " . $apat . " " . $amat;
                    echo $carg . " ". $rfc . " " . $curp;
                    echo $domi . " " . $tel . " " . $email;
                    echo $fn . " ". $ec . " " . $rf;
                    echo $obs;



                ?>

            </div>
		
            <div class="cplumn"></div>

        </div>

		

	<footer class="footer">
		<div class="content has-text-centered">
			<p>
				<strong> Developed by JesicaTeresaLG</strong>
				<a href="mapaches.info">mapaches.info</a>.
			</p>
		</div>
	</footer>
</body>
</html>
